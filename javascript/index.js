var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

// for debug
var printSend = false; // for printing all sended messages
var printRecieveCarPos = false; // for printing received carPos messages
var printRecieveOther = true; // for printing all received, other than carPos
var debugPrint = false; // for printing speed, target, angle, etc...

// setup
var createRace = false; // if true following setups are available, false sends normal join message to server
var numberOfCars = 1; // 1 = race alone (2+ waits opponents no password, etc..)
var trackName = "france"; // TEST TRACKS AVAILABLE: Finland (keimola), Germany (germany), U.S.A. (usa), France (france)

// for super AI
var lastPosition = 0;
var currentPosition = 0;
var lastSpeed = 0;
var currentSpeed = 0;
var lastAngle = 0;
var currentAngle = 0;
var accArray = new Array();
var speedArray = new Array();
var slope = -50;
var acceleration = 0;
var currentThrottle = 0;
var trackPieces = new Array();
var trackLanes = new Array();
var laneMap = new Array();
var targetSpeedArray = {};
var hC = Math.PI / 180;
var carIndex = -1;
var maxAngleLimit = 50;
var maxAngles = {};
var longestStraightBegin = 0;
var turboAvailable = false;
var switchSended = false;
var fineTuning = 0.0;

function targetSpeed(pieceIndex, inPieceDistance, zoneLength, speed)
{
    this.pieceIndex = pieceIndex;
    this.inPieceDistance = inPieceDistance;
    this.zoneLength = zoneLength;
    this.speed = speed;
}

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function ()
    {
        if (!createRace)
        {
            return send(
            {
                msgType : "join",
                data :
                {
                    name : botName,
                    key : botKey
                }
            }
            );
        }
        else
        {
            return send(
            {
                msgType : "createRace",
                data :
                {
                    botId :
                    {
                        name : botName,
                        key : botKey
                    },
                    trackName : trackName,
                    carCount : numberOfCars
                }
            }
            );
        }
    }
    );

function send(json)
{
    if (printSend)
        console.log("=> ", JSON.stringify(json));
    client.write(JSON.stringify(json));
    return client.write('\n');
};

function isBend(index)
{
    return (typeof(trackPieces[index].radius) != "undefined");
};

function isSwitch(index)
{
    return (typeof(trackPieces[index].switch) != "undefined");
};

function bendRadius(index, laneIndex)
{
    var piece = trackPieces[index];
    var disFromCenter = trackLanes[laneIndex].distanceFromCenter;

    return (isBend(index) ? (piece.radius + (piece.angle > 0 ? -1 : 1) * disFromCenter) : 0);
};

function getPieceDistance(index, laneIndex)
{
    var piece = trackPieces[index];
    var distance = 0;
    var disFromCenter = trackLanes[laneIndex].distanceFromCenter;

    if (isBend(index))
    {
        distance += bendRadius(index, laneIndex) * Math.abs(piece.angle) * hC;
    }
    else
    {
        distance += piece.length;
    }

    return distance;

};

function calculateLane(json)
{
	var laneIndex = json.data[carIndex].piecePosition.lane.startLaneIndex;
	var pieceIndex = json.data[carIndex].piecePosition.pieceIndex;
	var laneInMap = laneMap[pieceIndex + 1];
	if(laneInMap != null)
	{
		if(laneInMap != laneIndex && switchSended == false)
		{
			switchSended = true;
			return (laneInMap < laneIndex ? "Left" : "Right");
		}
	}
	else
		switchSended = false;
	return "forward";

};

function calculateTurboUsage(json)
{
    if (json.data[carIndex].piecePosition.pieceIndex == longestStraightBegin && turboAvailable == true)
    {
        turboAvailable = false;
        return "turbo";
    }
    else
        return "none";

};

function calculateDistanceToTarget(pieceIndex1, inPieceDistance1, pieceIndex2, inPieceDistance2, laneIndex)
{
    var distance = 0;
    // if target is in next lap
    if (pieceIndex1 > pieceIndex2)
        pieceIndex2 += trackPieces.length;

    for (var i = pieceIndex1; i <= pieceIndex2; i++)
    {
        // if target is in next lap check
        var piece = trackPieces[i % (trackPieces.length)];
        if (i == pieceIndex1)
        {
            // if in first piece, remove travelled distance, and add piece distance in next else
            distance -= inPieceDistance1;
        }

        if (i == pieceIndex2)
        {
            // if in last piece add distance to be travelled
            distance += inPieceDistance2;
        }
        else
        {
            // if in other than last piece, add piece distance
            distance += getPieceDistance(i % (trackPieces.length), laneIndex);
        }
    };
    return distance;
};

function getCarIndex(data)
{
    if (carIndex < 0 || carIndex >= data.length || data[carIndex].id.name !== botName)
    {
        for (var i = 0; i < data.length; i++)
        {
            if (data[i].id.name === botName)
            {
                carIndex = i;
                return;
            }
        }
    }
}

function calculateThrottle(json)
{

    var pieceIndex = json.data[carIndex].piecePosition.pieceIndex;
    var laneIndex = json.data[carIndex].piecePosition.lane.startLaneIndex;
    currentPosition = json.data[carIndex].piecePosition.inPieceDistance;
    currentAngle = json.data[carIndex].angle;
    currentSpeed = currentPosition - lastPosition;

    // TODO: after switch piece, speed is calculated wrong if switch is made
    // fix calculated speed, this happens when piece changes
    if (currentSpeed < 0)
    {
        // correct currentSpeed according previous trackPiece distance
        var prevIndex = (pieceIndex != 0 ? pieceIndex - 1 : trackPieces.length - 1);
        currentSpeed += getPieceDistance(prevIndex, laneIndex);
    }

    acceleration = currentSpeed - lastSpeed;

    var nextTarget;
    targetSpeedArray[laneIndex].forEach(function (target)
    {
        // pieceIndex in targetSpeedArray must be in order
        // to get next target speed
        if (target.pieceIndex >= pieceIndex && typeof(nextTarget) == "undefined")
        {
            // if in piece, but after inPieceDistance => get next target
            if (target.pieceIndex == pieceIndex && target.inPieceDistance < currentPosition)
                return;

            nextTarget =
                new targetSpeed(target.pieceIndex, target.inPieceDistance,
                    target.zoneLength, target.speed);
        }
    }
    );

    // if we do not have nextTarget we are in the end of lap so there are no targets left
    // so grap first target of lap
    if (typeof(nextTarget) == "undefined")
    {
        nextTarget = new targetSpeed(targetSpeedArray[laneIndex][0].pieceIndex,
                targetSpeedArray[laneIndex][0].inPieceDistance,
                targetSpeedArray[laneIndex][0].zoneLength, targetSpeedArray[laneIndex][0].speed);
    }

    if (isNaN(maxAngles[nextTarget.pieceIndex]))
        maxAngles[nextTarget.pieceIndex] = 0;
    // trying to save max angles, so next lap can be improved
    // not working properly because just after bend, angle can be largest value. so value may not be related to next bend
    maxAngles[nextTarget.pieceIndex] = Math.max(maxAngles[nextTarget.pieceIndex], Math.abs(currentAngle));

    if (debugPrint && false)
        console.log(currentSpeed,
            ", ", acceleration,
            ", ", nextTarget.speed.toFixed(2),
            ", ", json.gameTick);

    if ((debugPrint && true) || currentAngle > 50)
        console.log("speed:", currentSpeed.toFixed(2),
            //", nextTargetSpeed: ", nextTarget.speed.toFixed(2),
            //", nextTargetPiece: ", nextTarget.pieceIndex,
            //", nextTargetInPieceDistance: ", nextTarget.inPieceDistance.toFixed(2),
            //", acc: ", acceleration.toFixed(2),
            ", angle: ", currentAngle.toFixed(2),
            ", index: ", pieceIndex,
            //", isBend: ", isBend(pieceIndex),
            //", piecePos: ", currentPosition.toFixed(2),
            ", tick: ", json.gameTick,
            "");

    if (json.gameTick < 25 && acceleration < 0)
    {
        speedArray.push(currentSpeed);
        accArray.push(acceleration);

        if (speedArray.length == 10 && accArray.length == 10)
        {
            var slopeArray = new Array();
            for (var i = 0; i < 9; i++)
                slopeArray.push((speedArray[i] - speedArray[i + 1]) / (accArray[i] - accArray[i + 1]));
            var sum = eval(slopeArray.join('+'));
            slope = Math.abs(sum / slopeArray.length);
            console.log("SLOPE: ", slope);
        }
    }

    var dif = currentSpeed - nextTarget.speed;
    var calculatedThrottle = -1;

    var distanceToTargetEnd = calculateDistanceToTarget(pieceIndex, currentPosition, nextTarget.pieceIndex, nextTarget.inPieceDistance, laneIndex);

    if (dif > 0)
    {
        // if we have more speed than nextTarget.speed is,
        var breakingDistance = slope * dif;
        if (debugPrint)
			console.log("DTT: ", (distanceToTargetEnd - currentSpeed - nextTarget.zoneLength).toFixed(1), "BD: ", breakingDistance.toFixed(1))

        // break to zone start
        if (breakingDistance > distanceToTargetEnd - currentSpeed - nextTarget.zoneLength)
            calculatedThrottle = 0;
    }

    // we get here if we need more speed to target
    // or we have time to break before target
    if (calculatedThrottle == -1)
    {
        // if in zone maintain speed
        if (distanceToTargetEnd < nextTarget.zoneLength)
            calculatedThrottle = (Math.abs(currentAngle) > maxAngleLimit ? 0 : nextTarget.speed / 10);
        else if (Math.abs(currentAngle) > maxAngleLimit)
            calculatedThrottle = 0;
        else
            calculatedThrottle = 1;
    }

    lastPosition = currentPosition;
    lastSpeed = currentSpeed;
    lastAngle = currentAngle;

    return calculatedThrottle;
}

function computeTargetSpeeds()
{
    trackLanes.forEach(function (lane)
    {
        var bendLength = 0;
        for (var i = 0; i < trackPieces.length - 1; i++)
        {
            // save target speed for bends
            var radius = bendRadius(i, lane.index);
            var nextRadius = bendRadius(i + 1, lane.index);

            if (radius != 0)
            {
                bendLength += getPieceDistance(i, lane.index);
                // if we are at last piece of bend
                if (radius != nextRadius)
                {
                    if (bendLength > 40)
                    {
                        // for bend target speed is calculated according to excel graph.
                        targetSpeedArray[lane.index].push(
                            new targetSpeed(i, getPieceDistance(i, lane.index) / 2, bendLength * 0.75, radius / 30 + 3.2));
                    }
                    bendLength = 0;
                }
            }
        }
    }
    );
    console.log(targetSpeedArray);
}

function calculateBestTurboSpot()
{
    var maxStraight = 0;
    var maxStart = 0;
    var currentStraight = 0;
    var currentStart = -1;

    for (var i = 0; true; i++)
    {
        if (!isBend(i % trackPieces.length))
        {
            currentStraight += getPieceDistance(i % trackPieces.length, 0);

            if (currentStart == -1)
                currentStart = i;

            if (currentStraight > maxStraight)
            {
                maxStraight = currentStraight;
                maxStart = currentStart;
            }
        }
        else
        {
            if (i > trackPieces.length)
                break;

            currentStraight = 0;
            currentStart = -1;
        }
    }

    longestStraightBegin = maxStart;

}

function calculateLaneMap()
{
    //return;

    var i = 0;
    var startI = 0;
	var totalBend = 0;
    while (i < trackPieces.length)
    {
		var min = 0;
        startI = i;
        var lanesLength = new Array();
        for (var lane = 0; lane < trackLanes.length; lane++)
        {
            i = startI;
            while (!isSwitch(++i % trackPieces.length))
            {
                totalBend += getPieceDistance(i % trackPieces.length, lane);
            }
			lanesLength[lane] = totalBend;
			totalBend = 0;
        }
		if(lanesLength[0] == lanesLength[1]) continue;
		
		min = lanesLength.slice(0).sort()[0];
		minLane = lanesLength.indexOf(min);
		laneMap[startI] = minLane;
    }
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function (data)
{
    if (data.msgType !== 'undefined' && data.msgType === 'carPositions')
    {
        if (printRecieveCarPos)
            console.log("<= ", JSON.stringify(data));

        getCarIndex(data.data);

        var lane = calculateLane(data);
        var turbo = calculateTurboUsage(data);
        currentThrottle = calculateThrottle(data);
        if (currentThrottle < 0.0)
            currentThrottle = 0.0;
        if (currentThrottle > 1.0)
            currentThrottle = 1.0;

        if (data.gameTick < 10)
            currentThrottle = 1;
        else if (data.gameTick < 20)
            currentThrottle = 0;

        // if lane switch is selected, send it to server
        if (lane !== 'forward')
        {
            send(
            {
                msgType : "switchLane",
                data : lane,
                gameTick : data.gameTick
            }
            );
        }
        else if (turbo != 'none')
        {
            send(
            {
                msgType : "turbo",
                data : "Race was good but car was bad",
                gameTick : data.gameTick
            }
            );
        }
        else
        {
            // if lane switch is not done during this tick
            // send calculated throttle value
            send(
            {
                msgType : "throttle",
                data : currentThrottle,
                gameTick : data.gameTick
            }
            );
        }
    }
    else
    {
        if (printRecieveOther)
            console.log("<= ", JSON.stringify(data));

        if (data.msgType !== 'undefined' && data.msgType === 'join')
            console.log('Joined');
        else if (data.msgType !== 'undefined' && data.msgType === 'gameInit')
        {
            // save track to variables for later use
            trackPieces = data.data.race.track.pieces;
            trackLanes = data.data.race.track.lanes;
            cars = data.data.race.cars;

            trackLanes.forEach(function (lane)
            {
                targetSpeedArray[lane.index] = new Array();
            }
            );

            computeTargetSpeeds();
            calculateBestTurboSpot();
            calculateLaneMap();

            console.log('Race initialized');
        }
        else if (data.msgType !== 'undefined' && data.msgType === 'turboAvailable')
        {
            console.log('Turbo available');
            turboAvailable = true;
        }
        else if (data.msgType !== 'undefined' && data.msgType === 'gameStart')
        {
            console.log('Race started');
        }
		else if (data.msgType !== 'undefined' && data.msgType === 'crash')
		{
			//fineTuning = -0.3;
			//console.log(fineTuning);
        }
		else if (data.msgType !== 'undefined' && data.msgType === 'spawn')
		{
			turboAvailable = false;
		}
				
		else if (data.msgType !== 'undefined' && data.msgType === 'lapFinished')
 		{
			
        }
		else if (data.msgType !== 'undefined' && data.msgType === 'gameEnd')
        {
            console.log('Race ended');
            console.log(maxAngles);
        }

        send(
        {
            msgType : "ping",
            data : {}
        }
        );
    }
}
);

jsonStream.on('error', function ()
{
    return console.log("disconnected");
}
);
